package com.entrust.identityGuard.mobile.token;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.entrust.identityGuard.mobile.sdk.Identity;
import com.entrust.identityGuard.mobile.sdk.IdentityProvider;
import com.entrust.identityGuard.mobile.sdk.PlatformDelegate;
import com.google.gson.Gson;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EntrustHybridPlugin extends CordovaPlugin {

    private static final String TAG = "entrust";

    protected static Context context = null;

    private static final String FIDELITY_TOKEN_KEY="com.ng.fidelitybank.token.tokenJson";

    private static final String FIDELITY_TOKEN_SHARED_PREF="com.ng.fidelitybank.token.SharedPreference";

    private static final String GENERATE_TOKEN_ACTION = "generateToken";

    private static final String GENERATE_REG_CODE_ACTION = "generateRegistrationCode";

    private CordovaInterface mCordovaInterface;

    private SharedPreferences sharedpreferences;

    @Override
    public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
        Log.i(TAG, "EntrustPlugin::initialize");
        super.initialize(cordova, webView);
        context = super.cordova.getActivity().getApplicationContext();
        sharedpreferences = context.getSharedPreferences(FIDELITY_TOKEN_SHARED_PREF, Context.MODE_PRIVATE);
        PlatformDelegate.setLogLevel(PlatformDelegate.LOG_LEVEL_DEBUG);
        PlatformDelegate.initialize(context);
        PlatformDelegate.setApplicationScheme("entrust");

        mCordovaInterface = cordova;
        mCordovaInterface.setActivityResultCallback(this);

    }
    /**
     * Entry point for JavaScript calls.
     */
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        Log.v(TAG, "Executing action: " + action);
        switch (action) {
            case GENERATE_TOKEN_ACTION:
                generateToken(callbackContext);
                break;
            case GENERATE_REG_CODE_ACTION:
                generateRegistrationCode(args, callbackContext);
                break;
            default:
                return false;
        }
        return true;
    }


    private void generateRegistrationCode(JSONArray args, CallbackContext callbackContext) {
        Log.i(TAG, "generateRegistrationCode");
        try {
            final JSONObject options = args.getJSONObject(0);

            final String deviceID = options.getString("deviceID");
            final String serialNumber = options.getString("serialNumber");
            final String activationCode = options.getString("activationCode");

            Identity identity = new IdentityProvider().generate(deviceID,serialNumber, activationCode);

            SharedPreferences.Editor editor = sharedpreferences.edit();

            Token token= new Token();
            token.setSeed(identity.getSeed());
            token.setActivationCode(activationCode);
            token.setSerialNumber(serialNumber);
            token.setDeviceId(deviceID);

            Gson gson = new Gson();
            String tokenJson = gson.toJson(token);
            editor.putString(FIDELITY_TOKEN_KEY, tokenJson);
            editor.commit();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, identity.getRegistrationCode()));
        } catch (Exception e) {
            Log.e(TAG,"Error while generating registration code",e);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
        }
        return;
    }


    private void generateToken(CallbackContext callbackContext) {
        Log.i(TAG, "generateToken");
        try {
            Gson gson = new Gson();
            SharedPreferences prefs = sharedpreferences;
            String tokenJson = prefs.getString(FIDELITY_TOKEN_KEY, null);
            Token token= gson.fromJson(tokenJson, Token.class);
            Log.i(TAG, "token::"+tokenJson);
            Identity identity = new IdentityProvider().generate(token.getDeviceId(),token.getSerialNumber(), token.getActivationCode());
            identity.setSeed(token.getSeed());
            Log.i(TAG, "identity.getSeed::"+identity.getSeed());
            Log.i(TAG, "identity.getOTP::"+identity.getOTP());
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, identity.getOTP()));
        } catch (Exception e) {
            Log.e(TAG,"Error while generating OTP",e);
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.getMessage()));
        }
        return;
    }
}