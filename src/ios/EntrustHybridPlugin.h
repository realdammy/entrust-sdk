/**
Entrust implementation header file
**/
#import <Cordova/CDVPlugin.h>

@interface EntrustHybridPlugin : CDVPlugin

- (void)generateToken:(CDVInvokedUrlCommand*)command;
- (void)generateRegistrationCode:(CDVInvokedUrlCommand*)command;

@end
