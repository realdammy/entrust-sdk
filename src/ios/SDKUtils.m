//
//  SDKUtils.m
//  Entrust IdentityGuard Mobile SDK
//  Command Line Example
//
//  Copyright (c) 2013 Entrust, Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import "SDKUtils.h"
#import "ETConfigurationFile.h"
#import "ETIdentityProvider.h"
#import "ETSoftTokenSDK.h"

#define kFileName @"fidelityBankSoftTokenIdentity"

@implementation SDKUtils

static NSString *dataFileName;

/**
 * Gets the file name where the soft token identity will be stored.
 * @return The file name where the soft token identity will be stored.
 */
+ (NSString *)getIdentityFileName
{
    if (dataFileName == nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        dataFileName = [documentsDirectory stringByAppendingPathComponent:kFileName];
    }
    return dataFileName;
}

/**
 * Saves the current identity to disk.
 * @param identity The identity to save.
 * @return YES on success, NO otherwise.
 */
+ (BOOL) saveIdentity:(ETIdentity *)identity
{
    NSData *serialized = [NSKeyedArchiver archivedDataWithRootObject:identity];
    NSData *encrypted = [ETSoftTokenSDK encryptData:serialized];
    return [encrypted writeToFile:[SDKUtils getIdentityFileName] atomically:YES];
}

/**
 * Loads the identity from disk.
 * @return The identity from disk or nil if no identity exists.
 */
+ (ETIdentity *)loadIdentity
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:[SDKUtils getIdentityFileName]]) {
        return nil;
    }
    printf("File: %s\n", [[SDKUtils getIdentityFileName] UTF8String]);
    NSData *encrypted = [[NSData alloc] initWithContentsOfFile:[SDKUtils getIdentityFileName]];
    NSData *serialized = [ETSoftTokenSDK decryptData:encrypted];
    return [NSKeyedUnarchiver unarchiveObjectWithData:serialized];
}

/**
 * Deletes the current identity file from disk.
 * @return YES on success, false otherwise.
 */
+ (BOOL) deleteIdentityFile
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[SDKUtils getIdentityFileName]]) {
        printf("Removed existing identity state file.\n\n");
        return [[NSFileManager defaultManager] removeItemAtPath:[SDKUtils getIdentityFileName] error:nil];
    }
    return NO;
}

@end
