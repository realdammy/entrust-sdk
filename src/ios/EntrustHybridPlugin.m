#import "EntrustHybridPlugin.h"
#import <Cordova/CDV.h>
#import "ETIdentity.h"
#import "ETIdentityProvider.h"
#import "SDKUtils.h"

@implementation EntrustHybridPlugin

-(void) generateRegistrationCode:(CDVInvokedUrlCommand*)command{

  NSDictionary* options = [command argumentAtIndex:0];
  NSString *deviceID  = options[@"deviceID"];
  NSString *serialNumber = options[@"serialNumber"];
  NSString *activationCode = options[@"activationCode"];

  @try {
  ETIdentity *tmpIdentity = [ETIdentityProvider generate:deviceID
                                              serialNumber:serialNumber
                                            activationCode:activationCode];

      /*NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
      [defaults setObject:tmpIdentity.seed forKey:@"fbt_entrust_seed"];
      [defaults setObject:serialNumber forKey:@"fbt_entrust_serialNumber"];
      [defaults setObject:activationCode forKey:@"fbt_entrust_activationCode"];
      [defaults setObject:deviceID forKey:@"fbt_entrust_deviceID"];
      [defaults synchronize];*/
      [SDKUtils saveIdentity:tmpIdentity];

      CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:tmpIdentity.registrationCode];
      [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    } @catch (NSException *e) {
      CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not generate registration code"];
       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
       return;
    }
  //NSString *userName=[defaults ObjectforKey:@"userName"];
}


-(void) generateToken:(CDVInvokedUrlCommand*)command{

  /*NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  //NSString *seed=[defaults ObjectforKey:@"fbt_entrust_seed"];
  NSString *deviceID=[defaults objectForKey:@"fbt_entrust_deviceID"];
  NSString *serialNumber=[defaults objectForKey:@"fbt_entrust_serialNumber"];
  NSString *activationCode=[defaults objectForKey:@"fbt_entrust_activationCode"];*/

  @try {
  ETIdentity *identity = [SDKUtils loadIdentity];
  /*ETIdentity *tmpIdentity = [ETIdentityProvider generate:deviceID // Not registering online during this step so don't provide a device ID.
                                              serialNumber:serialNumber
                                            activationCode:activationCode];

      [tmpIdentity seed:seed];*/

      NSString *otpString = [identity getOTP:[NSDate date]];

      CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:otpString];
      [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

    } @catch (NSException *e) {
      CDVPluginResult * pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Could not generate OTP"];
       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
       return;
    }
}
@end
