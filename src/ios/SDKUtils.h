//
//  SDKUtils.h
//  Entrust IdentityGuard Mobile SDK
//  Command Line Example
//
//  Copyright (c) 2013 Entrust, Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETIdentity.h"
#import "ETConfigurationFile.h"

@interface SDKUtils : NSObject

/**
 * Gets the file name where the soft token identity will be stored.
 * @return The file name where the soft token identity will be stored.
 */
+ (NSString *)getIdentityFileName;

/**
 * Saves the current identity to disk.
 * @param identity The identity to save.
 * @return YES on success, NO otherwise.
 */
+ (BOOL) saveIdentity:(ETIdentity *)identity;

/**
 * Loads the identity from disk.
 * @return The identity from disk or nil if no identity exists.
 */
+ (ETIdentity *)loadIdentity;

/**
 * Deletes the current identity file from disk.
 * @return YES on success, false otherwise.
 */
+ (BOOL) deleteIdentityFile;

@end
