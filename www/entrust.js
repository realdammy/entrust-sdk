/*
module.exports = {
    testACallJs: function (name, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "EntrustHybridPlugin", "testACall", [name]);
    }
};*/
var exec = require('cordova/exec');


var Entrust = {
  generateToken: function (onSuccess,onFailure){
    exec(onSuccess, onFailure, "EntrustHybridPlugin", "generateToken", []);
  },

  generateRegistrationCode: function (deviceID, serialNumber,activationCode, onSuccess,onFailure){
    var objToSend={};
    objToSend.deviceID= deviceID;
    objToSend.serialNumber= serialNumber;
    objToSend.activationCode=activationCode;
    exec(onSuccess, onFailure, "EntrustHybridPlugin", "generateRegistrationCode", [objToSend]);
  }
}

module.exports = Entrust;
